import java.sql.*;
// this is the JDBC library of classes to perform the built methods and classes of java.sql

public class Connect {
	
	static Connection con;
	static Statement st;
	
	public Connect() {
		connect();
	}
	
	public static void connect() {
		try {
			Class.forName("org.postgresql.Driver");
			// this method is used to load the JBDC driver class
			
			System.out.println("Driver Loaded Successfully");
			
			con = DriverManager.getConnection("jdbc:postgresql://localhost/BOOK", "postgres", "gael2131");
			/* this method is used to create a Connection object called con
			 * this is the format: Connection con=DriverManager.getConnection(URL, UserName, Password) 
			 * Connection Parameters:
			 * 1. URL - WIth JBDC databases are represented by a URL (Uniform Resource Locator) with PostgreSQL 
			 * the format is jdbc:postgresql://host:port/database 
			 * host: Database server IP or Name in order to specify an IPv6 address you must enclose the host parameter with square brackets
			 * port: port number the server is listening on defaults to standard port number (5432)
			 * database: the database name
			 * 2. UserName: The database user on whose behalf the connection is being made
			 * 3. Password: The database user password
			*/  
			
			System.out.println("Successful Connection");
			
			st = con.createStatement();
			// basic statement class Statement this is how a statement object is created using createStatement function of the Connection class of JDBC
			
			System.out.println("Statement created Successfully");
			System.out.println("Now, You can Access the DataBase");
		
		}catch (ClassNotFoundException cnfe) {
			System.err.println(cnfe);
		
		}catch (SQLException sqle) {
		System.err.println(sqle);
	}


}
}
